package net.eldiosantos.poc.apollo;

import com.google.inject.Inject;
import com.spotify.apollo.Environment;
import com.spotify.apollo.RequestContext;
import com.spotify.apollo.Response;
import com.spotify.apollo.httpservice.HttpService;
import com.spotify.apollo.httpservice.LoadingException;
import com.spotify.apollo.route.Route;
import net.eldiosantos.poc.service.UptimeService;
import org.jboss.weld.environment.se.bindings.Parameters;
import org.jboss.weld.environment.se.events.ContainerInitialized;

import javax.enterprise.event.Observes;
import java.util.Date;
import java.util.List;

/**
 * Created by esjunior on 30/11/2016.
 */
public class MainAppClass {

    @Inject
    private UptimeService uptimeService;

    public void main(@Observes ContainerInitialized event, @Parameters List<String> parameters) throws LoadingException {
        HttpService.boot(this::init, "my-app", parameters.toArray(new String[0]));
    }

    void init(Environment environment) {
        environment.routingEngine()
                .registerAutoRoute(Route.sync("GET", "/", rc -> new Date(uptimeService.getStartedAt().toEpochMilli())));
    }
}
