package net.eldiosantos.poc.spark;

import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import net.eldiosantos.poc.dto.PersonDTO;
import net.eldiosantos.poc.service.FindPerson;
import net.eldiosantos.poc.service.InsertPerson;
import net.eldiosantos.poc.service.InsertRequestLog;
import net.eldiosantos.poc.service.ListPerson;
import org.jboss.weld.environment.se.bindings.Parameters;
import org.jboss.weld.environment.se.events.ContainerInitialized;

import javax.enterprise.event.Observes;
import javax.inject.Inject;
import java.util.List;

import static spark.Spark.*;

/**
 * Created by Eldius on 04/12/2016.
 */
@Slf4j
public class SparkMainClass {

    @Inject
    private InsertRequestLog insertRequestLog;

    @Inject
    private InsertPerson insertPerson;

    @Inject
    private ListPerson listPerson;

    @Inject
    private FindPerson findPerson;

    public void main(@Observes ContainerInitialized event, @Parameters List<String> parameters) {

        final Gson gson = new Gson();

        port(Integer.valueOf(System.getProperty("server.port", "8080")));
        before((request, response) -> {
            log.info("# Request received: ###########################");
            log.info(request.headers("Authorization"));
            log.info("###############################################");
        });

        after((request, response) -> {
            response.header("built-with", "spark-java");
            response.header("Content-Encoding", "gzip");;
            response.type("application/json");
        });

        post("/person", (req, res) -> gson.toJson(insertPerson.insert(gson.fromJson(req.body(), PersonDTO.class))));

        get("/person", (request, response) -> gson.toJson(listPerson.list()));

        get("/person/:id", (request, response) -> gson.toJson(findPerson.find(Long.valueOf(request.params("id")))));
    }
}
