package net.eldiosantos.poc.client.domain

import scala.annotation.tailrec
import scala.collection.mutable

/**
  * Created by esjunior on 29/12/2016.
  */
class Project(_name: String, _path: String, _port: Int) {
  val name = _name
  val path = _path
  val port = _port
}

object Project {

  val projectList: mutable.Set[Project] = mutable.Set()
  val r = scala.util.Random

  def apply(_name: String, _path: String, _port: Int): Project = {
    val project = new Project(_name, _path, _port)
    projectList += project
    project
  }
  def apply(_name: String, _path: String): Project = {

    val project = new Project(_name, _path, getRandomPort())
    projectList += project
    project
  }

  def verifyPortIsUsed(port: Int): Boolean = projectList.find(_.port.equals(port)).nonEmpty

  @tailrec
  def getRandomPort(): Int = {
    val port = r.nextInt(60000) + 2000
    if(verifyPortIsUsed(port)) getRandomPort()
    else port
  }
}
