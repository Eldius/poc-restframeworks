package net.eldiosantos.poc.client.task

import java.io.File

import net.eldiosantos.poc.client.domain.Project

/**
  * Created by esjunior on 29/12/2016.
  */
class ProjectListService(val rootPath: String) {
  def listProjects(): Set[Project] = {
    new File(rootPath)
      .listFiles
      .filter(_.getName.startsWith("poc-"))
      .map(f => Project(f.getName, f.getAbsolutePath))
      .toSet
  }
}
