package net.eldiosantos.poc.client.task

import java.io.File

import net.eldiosantos.poc.client.domain.Project

import scala.sys.process.{Process, ProcessLogger}

/**
  * Created by esjunior on 29/12/2016.
  */
class StartService {

  def start(project: Project): Unit = {
    val script = new File(s"${project.path}/build/unpacked/").listFiles
      .find(f => {
        println(s"first flag: ${f.getAbsolutePath}")
        f.getName.toUpperCase.startsWith(project.name.toUpperCase)
      }).get.listFiles
      .find(f => {
        println(s"second flag: ${f.getAbsolutePath}")
        println(s"second flag: ${f.getName}")
        f.getName.equals("bin")
      }).get.listFiles
      .find(f => {
        println(s"third flag: ${f.getAbsolutePath}")
        f.getName.equals(project.name)
      }).get

    println(s"running '${project.name}' => ${script.getAbsolutePath}")
    //val process = Runtime.getRuntime.exec(script.getAbsolutePath)

    val process = Process(script.getAbsolutePath).run(ProcessLogger(l => println(s"${project.name}: $l"), l => println(s"${project.name}:error: $l")))

    Thread.sleep(120000)

    process.destroy()

    println(s"stopping '${project.name}' => ${script.getAbsolutePath}")
  }
}

object StartService {
  val service = new StartService
  def apply(project: Project) = service.start(project)
}
