package net.eldiosantos.poc.client.domain

/**
  * Created by esjunior on 23/12/2016.
  */
class RequestLog(_start: Long, _finish: Long, _tag: String, _startTag: Long, _finishTag: Long) {
  val start = _start
  val finish = _finish
  val tag = _tag
  val startTag = _startTag
  val finishTag = _finishTag
}

object RequestLog {
  def apply(start: Long, finish: Long, tag: String, startTag: Long, finishTag: Long): RequestLog = new RequestLog(start, finish, tag, startTag, finishTag)
  def apply(start: Long, finish: Long, tag: String): RequestLog = new RequestLog(start, finish, tag, -1, -1)
}
