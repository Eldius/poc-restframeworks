package net.eldiosantos.poc.client.database

//import org.scalarelational.h2.{H2Datastore, H2Embedded}

import java.sql.{DriverManager, ResultSet}

import net.eldiosantos.poc.client.config.ConfigurationObject
import net.eldiosantos.poc.client.domain.RequestLog
/**
  * Created by esjunior on 23/12/2016.
  */
object DatabaseObject {
  val config = ConfigurationObject.config().getConfig("database")

  Class.forName(config.getString("driver"))
  val connection = DriverManager.getConnection(config.getString("url"), config.getString("user"), config.getString("pass") )

  def results[T](resultSet: ResultSet)(f: ResultSet => T) = {
    new Iterator[T] {
      def hasNext = resultSet.next()
      def next() = f(resultSet)
    }
  }

  object RequestHistory {
    val insert = "insert into request_history (start, finish, tag, startTag, finishTag) values (?, ?, ?, ?, ?)"
    val select = "select id, start, finish, tag, startTag, finishTag from request_history"
    def insert(request: RequestLog): Unit = {
      val st = connection.prepareStatement(this.insert)
      st.setLong(1, request.start)
      st.setLong(2, request.finish)
      st.setString(3, request.tag)
      st.setLong(4, request.startTag)
      st.setLong(5, request.finishTag)

      st.execute()
    }

    def listRequestLog(): Set[RequestLog] = {
      results(connection.createStatement().executeQuery(select)) { rs =>
        1.to(6).foreach( i => {
          print(s"value[$i]: ${rs.getObject(i)}")
          println()
        })
        RequestLog(rs.getLong(2), rs.getLong(3), rs.getString(4), rs.getLong(5), rs.getLong(6))
      }.toSet
    }
  }

  def createSchema(): Unit = {
    connection.createStatement().execute(config.getString("create"))
  }
}