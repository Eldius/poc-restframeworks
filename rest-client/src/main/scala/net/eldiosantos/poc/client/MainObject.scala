package net.eldiosantos.poc.client

import net.eldiosantos.poc.client.database.DatabaseObject
import net.eldiosantos.poc.client.task.{ProjectListService, StartService}

object MainObject extends App{
  println("Running...")

  DatabaseObject.createSchema()

  new ProjectListService(".").listProjects().foreach {p => StartService(p)}

  println("Finished execution")
}
