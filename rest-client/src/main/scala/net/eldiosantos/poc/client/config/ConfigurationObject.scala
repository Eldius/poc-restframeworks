package net.eldiosantos.poc.client.config

import com.typesafe.config.{Config, ConfigParseOptions, ConfigSyntax}

/**
  * Created by esjunior on 22/12/2016.
  */
object ConfigurationObject {

  import com.typesafe.config.ConfigFactory
  val options = ConfigParseOptions.defaults()
           .setSyntax(ConfigSyntax.CONF)
           .setAllowMissing(false)
  val configuration = ConfigFactory.load(options)

  def config(): Config = {
    configuration
  }
}
