package net.eldiosantos.poc.client.database

import org.scalatest.{BeforeAndAfterEach, FlatSpec, FunSuite, Matchers}

/**
  * Created by esjunior on 23/12/2016.
  */
class DatabaseObject$Test extends FlatSpec with Matchers {

  it should "be able to create database" in {
    DatabaseObject.createSchema()
  }

}
