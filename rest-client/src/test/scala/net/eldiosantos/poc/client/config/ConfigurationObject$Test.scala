package net.eldiosantos.poc.client.config

import org.scalatest.{BeforeAndAfterEach, FlatSpec, FunSuite, Matchers}

/**
  * Created by esjunior on 23/12/2016.
  */
class ConfigurationObject$Test extends FlatSpec with Matchers {

  it should "be able to find database info" in {
    val databaseUrl = ConfigurationObject.config().getConfig("database").getString("url")

    databaseUrl should be ("jdbc:h2:./build/test.db")

  }

}
