package net.eldiosantos.poc.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Created by Eldius on 04/12/2016.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RequestLogDTO {
    private Long id;
    private Date startTime;
    private Date endDate;
    private Long startTimeLong;
    private Long endDateLong;

}
