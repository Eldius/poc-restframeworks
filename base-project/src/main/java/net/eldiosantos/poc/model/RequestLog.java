package net.eldiosantos.poc.model;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Eldius on 04/12/2016.
 */
@Data
@EqualsAndHashCode
@Builder
@Entity
public class RequestLog {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    private Date startTime;

    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;

    private String authorizationToken;

    private Long startTimeLong;
    private Long endDateLong;

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
        this.startTimeLong = startTime.getTime();
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
        this.endDateLong = endDate.getTime();
    }
}
