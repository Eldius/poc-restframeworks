package net.eldiosantos.poc.model;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by esjunior on 30/11/2016.
 */
@Data
@EqualsAndHashCode
@Builder
@Entity
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    private String name;
    @Temporal(TemporalType.DATE)
    private Date birthDate;
}
