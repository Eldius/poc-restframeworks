package net.eldiosantos.poc.service;

import net.eldiosantos.poc.dto.PersonDTO;
import net.eldiosantos.poc.model.Person;
import net.eldiosantos.poc.repository.PersonRepository;
import org.modelmapper.ModelMapper;

import javax.inject.Inject;

/**
 * Created by Eldius on 04/12/2016.
 */
public class UpdatePerson {

    @Inject
    private PersonRepository repository;

    @Inject
    private ModelMapper modelMapper;

    @Deprecated
    public UpdatePerson() {
    }

    public UpdatePerson(PersonRepository repository, ModelMapper modelMapper) {
        this.repository = repository;
        this.modelMapper = modelMapper;
    }

    public PersonDTO update(final PersonDTO person) {
        return modelMapper.map(repository.update(modelMapper.map(person, Person.class)), PersonDTO.class);
    }
}
