package net.eldiosantos.poc.service;

import net.eldiosantos.poc.dto.RequestLogDTO;
import net.eldiosantos.poc.model.RequestLog;
import net.eldiosantos.poc.repository.RequestLogRepository;
import org.modelmapper.ModelMapper;

import javax.inject.Inject;

/**
 * Created by Eldius on 04/12/2016.
 */
public class InsertRequestLog {

    @Inject
    private RequestLogRepository repository;

    @Inject
    private ModelMapper modelMapper;

    @Deprecated
    public InsertRequestLog() {
    }

    public InsertRequestLog(RequestLogRepository repository, ModelMapper modelMapper) {
        this.repository = repository;
        this.modelMapper = modelMapper;
    }

    public RequestLogDTO insert(final RequestLog log) {
        return modelMapper.map(repository.insert(log), RequestLogDTO.class);
    }
}
