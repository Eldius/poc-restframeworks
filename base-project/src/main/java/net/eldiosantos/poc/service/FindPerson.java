package net.eldiosantos.poc.service;

import net.eldiosantos.poc.dto.PersonDTO;
import net.eldiosantos.poc.repository.PersonRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by Eldius on 04/12/2016.
 */
public class FindPerson {

    @Inject
    private PersonRepository repository;

    @Inject
    private ModelMapper modelMapper;

    @Deprecated
    public FindPerson() {
    }

    public FindPerson(PersonRepository repository, ModelMapper modelMapper) {
        this.repository = repository;
        this.modelMapper = modelMapper;
    }

    public PersonDTO find(Long id) {
        return modelMapper.map(repository.findByPk(id), PersonDTO.class);
    }
}
