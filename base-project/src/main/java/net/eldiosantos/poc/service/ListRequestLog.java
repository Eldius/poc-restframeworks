package net.eldiosantos.poc.service;

import net.eldiosantos.poc.model.RequestLog;
import net.eldiosantos.poc.repository.RequestLogRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by Eldius on 04/12/2016.
 */
public class ListRequestLog {

    @Inject
    private RequestLogRepository repository;

    @Inject
    private ModelMapper mapper;

    @Deprecated
    public ListRequestLog() {
    }

    public ListRequestLog(RequestLogRepository repository, ModelMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    public List<RequestLog>list() {
        return mapper.map(repository.findAll(), new TypeToken<List<RequestLog>>(){}.getType());
    }
}
