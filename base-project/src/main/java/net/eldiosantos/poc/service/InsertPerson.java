package net.eldiosantos.poc.service;

import net.eldiosantos.poc.dto.PersonDTO;
import net.eldiosantos.poc.model.Person;
import net.eldiosantos.poc.repository.PersonRepository;
import org.modelmapper.ModelMapper;

import javax.inject.Inject;

/**
 * Created by Eldius on 04/12/2016.
 */
public class InsertPerson {

    @Inject
    private PersonRepository repository;

    @Inject
    private ModelMapper modelMapper;

    @Deprecated
    public InsertPerson() {
    }

    public InsertPerson(PersonRepository repository, ModelMapper modelMapper) {
        this.repository = repository;
        this.modelMapper = modelMapper;
    }

    public PersonDTO insert(final PersonDTO person) {
        return modelMapper.map(repository.insert(modelMapper.map(person, Person.class)), PersonDTO.class);
    }
}
