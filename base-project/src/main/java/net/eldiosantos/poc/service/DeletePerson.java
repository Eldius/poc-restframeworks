package net.eldiosantos.poc.service;

import net.eldiosantos.poc.dto.PersonDTO;
import net.eldiosantos.poc.model.Person;
import net.eldiosantos.poc.repository.PersonRepository;
import org.modelmapper.ModelMapper;

import javax.inject.Inject;

/**
 * Created by Eldius on 04/12/2016.
 */
public class DeletePerson {

    @Inject
    private PersonRepository repository;

    @Inject
    private ModelMapper modelMapper;

    @Deprecated
    public DeletePerson() {
    }

    public DeletePerson(PersonRepository repository, ModelMapper modelMapper) {
        this.repository = repository;
        this.modelMapper = modelMapper;
    }

    public void delete(final PersonDTO person) {
        repository.delete(repository.findByPk(person.getId()));
    }
}
