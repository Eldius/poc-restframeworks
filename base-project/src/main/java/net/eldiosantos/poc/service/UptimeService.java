package net.eldiosantos.poc.service;

import javax.enterprise.context.ApplicationScoped;
import java.time.Instant;

/**
 * Created by esjunior on 30/11/2016.
 */
@ApplicationScoped
public class UptimeService {
    private final Instant startedAt = Instant.now();

    public Instant getStartedAt() {
        return startedAt;
    }
}
