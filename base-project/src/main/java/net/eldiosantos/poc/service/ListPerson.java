package net.eldiosantos.poc.service;

import net.eldiosantos.poc.dto.PersonDTO;
import net.eldiosantos.poc.model.Person;
import net.eldiosantos.poc.repository.PersonRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;

import javax.inject.Inject;
import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by Eldius on 04/12/2016.
 */
public class ListPerson {

    @Inject
    private PersonRepository repository;

    @Inject
    private ModelMapper modelMapper;

    @Deprecated
    public ListPerson() {
    }

    public ListPerson(PersonRepository repository, ModelMapper modelMapper) {
        this.repository = repository;
        this.modelMapper = modelMapper;
    }

    public List<PersonDTO> list() {
        return modelMapper.map(repository.findAll(), new TypeToken<List<PersonDTO>>(){}.getType());
    }
}
