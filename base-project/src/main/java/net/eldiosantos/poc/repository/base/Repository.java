package net.eldiosantos.poc.repository.base;

import java.io.Serializable;

/**
 * Created by Eldius on 27/11/2016.
 */
public interface Repository<T, K extends Serializable> extends ReadRepository<T, K>, WriteRepository<T, K> {
}
