package net.eldiosantos.poc.repository.base;

import java.io.Serializable;
import java.util.List;

/**
 * Unify all the write behaviours.
 */
public interface WriteRepository<T, K extends Serializable>
        extends InsertRepository<T, K>
        , UpdateRepository<T, K>
        , DeleteRepository<T, K> {

    /**
     * Save or update the object.
     * @param object object to be saved.
     * @return the object identification.
     */
    K saveOrUpdate(T object);

    /**
     * Save or update the object list.
     * @param object object to be saved.
     * @return the objects' identification list.
     */
    List<K> saveOrUpdate(List<T> object);
}
