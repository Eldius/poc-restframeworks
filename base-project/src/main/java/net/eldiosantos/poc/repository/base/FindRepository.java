package net.eldiosantos.poc.repository.base;

import java.io.Serializable;
import java.util.List;

/**
 * Definition of find methods for the repositories.
 */
public interface FindRepository<T, K extends Serializable>  {

    /**
     * Find the object by it's identifier.
     * @param pk object identifier.
     * @return the object itself.
     */
    T findByPk(K pk);

    /**
     * Find all the objects.
     * (Try to not release the - memory - Kraken)
     * @return all the objects.
     */
    List<T>findAll();
}
