package net.eldiosantos.poc.repository.base;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 * The base Hibernate implementation.
 */
public abstract class HibernateRepositoryImpl<T, K extends Serializable> {

	@Inject
	private EntityManager entityManager;

    private final Class<T>clazz;

	@Deprecated
    public HibernateRepositoryImpl() {
		super();
        this.clazz = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}

	public HibernateRepositoryImpl(EntityManager entityManager) {
		super();
        this.clazz = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
		this.entityManager = entityManager;
	}

	public void delete(T object) {

    }

    public T insert(T object) {
        entityManager.persist(object);
        return object;
    }

    public T findByPk(K id) {
        return entityManager.find(clazz, id);
    }

    public List<T> findAll() {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> cq = (CriteriaQuery<T>) cb.createQuery(clazz);
        Root<T> rootEntry = (Root<T>) cq.from(clazz);
        CriteriaQuery<T> all = cq.select(rootEntry);
        TypedQuery<T> allQuery = entityManager.createQuery(all);
        return allQuery.getResultList();
    }

    public T update(T object) {
        entityManager.merge(object);
        return object;
    }

}
