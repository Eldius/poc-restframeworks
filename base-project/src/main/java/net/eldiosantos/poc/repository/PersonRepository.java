package net.eldiosantos.poc.repository;

import net.eldiosantos.poc.model.Person;
import net.eldiosantos.poc.repository.base.HibernateRepositoryImpl;

/**
 * Created by Eldius on 04/12/2016.
 */
public class PersonRepository extends HibernateRepositoryImpl<Person,Long> {
}
