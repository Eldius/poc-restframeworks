package net.eldiosantos.poc.repository.base;

import java.io.Serializable;

/**
 * Created by Eldius on 27/11/2016.
 */
public interface ReadRepository<T, K extends Serializable> extends FindRepository<T, K>  {
}
