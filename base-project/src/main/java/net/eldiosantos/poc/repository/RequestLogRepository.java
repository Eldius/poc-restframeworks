package net.eldiosantos.poc.repository;

import net.eldiosantos.poc.model.RequestLog;
import net.eldiosantos.poc.repository.base.HibernateRepositoryImpl;

/**
 * Created by Eldius on 04/12/2016.
 */
public class RequestLogRepository extends HibernateRepositoryImpl<RequestLog,Long> {
}
