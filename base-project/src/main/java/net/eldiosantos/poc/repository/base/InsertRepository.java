package net.eldiosantos.poc.repository.base;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Eldius on 27/11/2016.
 */
interface InsertRepository <T, K extends Serializable> {

    /**
     * Insert an object.
     * @param object the object to be saved.
     * @return the object identification.
     */
    T insert(T object);

    /**
     * Insert a list of objects.
     * @param list a list of objects to be persisted.
     * @return list of the persisted objects.
     */
    List<T>insert(List<T> list);
}
