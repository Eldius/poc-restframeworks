package net.eldiosantos.poc.repository.base;

import java.io.Serializable;

/**
 * Interface to define the removable entity repository.
 */
public interface DeleteRepository<T, K extends Serializable> {
    /**
     * Deletes the object itself.
     * @param object the object to be removed.
     */
    void delete(T object);

    /**
     * Deletes the object identified by the pk parameter.
     * @param pk identification of object (a primary key).
     */
    void delete(K pk);

    /**
     * Empty the table.
     * (Try to not destroy all the data, please)
     */
    void delete();
}
