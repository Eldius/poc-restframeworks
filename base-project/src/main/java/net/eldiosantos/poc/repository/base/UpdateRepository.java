package net.eldiosantos.poc.repository.base;

import java.io.Serializable;
import java.util.List;

/**
 * Define the updatable objects' repository behaviour.
 */
public interface UpdateRepository<T, K extends Serializable> {

    /**
     * Update the object.
     * @param object object to be updated.
     * @return the object's identification.
     */
    K update(T object);

    /**
     * Update the list of objects.
     * @param list a list of objects.
     * @return the objects' identification list.
     */
    List<K> update(List<T> list);
}
