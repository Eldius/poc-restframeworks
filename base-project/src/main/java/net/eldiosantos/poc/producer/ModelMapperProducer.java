package net.eldiosantos.poc.producer;

import org.modelmapper.ModelMapper;
/**
 * Created by Eldius on 04/12/2016.
 */
public class ModelMapperProducer {
    public ModelMapper produce() {
        return new ModelMapper();
    }
}
